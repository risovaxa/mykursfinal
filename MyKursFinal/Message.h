#pragma once
#include <string>
class Message
{
public:
	Message();
	Message( std::string from,  std::string to,  std::string subject,  std::string date);
	
	~Message();

	 std::string getFrom() ;
	 std::string getTo() ;
	 std::string getSubject() ;
	 std::string getDate() ;
	
	void setFrom();
	void setTo();
	void setSubject();
	void setDate();
	
	
	
private:
	std::string _from;
	std::string _to;
	std::string _subject;
	std::string _date;

};

