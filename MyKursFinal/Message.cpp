#include "stdafx.h"
#include "Message.h"
#include <iostream>
#include <stdio.h>      
#include <ctime>

Message::Message()
{
	_from = "defFrom";
	_to = "defTo";
	_subject = "defSubject";
	_date = "defTime";
}

Message::Message( std::string from, 
				  std::string to, 
				  std::string subject,
				  std::string date)
	: _from(from)
	, _to(to)
	, _subject(subject)
	, _date(date) {}

 std::string Message::getFrom()  {
	return _from;
}

 std::string Message::getTo()  
{
	return _to;
}

 std::string Message::getSubject()  
{
	return _subject;
}

 std::string Message::getDate()  
{
	return _date;
}

void Message::setFrom() 
{
	std::cout << "Enter ur email: ";
	std::cin >> _from;
}

void Message::setTo() 
{
	std::cout << "Enter who u want to send email: ";
	std::cin >> _to;
}

void Message::setSubject() 
{
	std::cout << "Enter subject: ";
	std::cin >> _subject;
}

void Message::setDate() 
{
	std::cout << "Enter date: ";
	std::cin >> _date;
}

Message::~Message(){}
