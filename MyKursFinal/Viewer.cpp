#include "stdafx.h"
#include "Viewer.h"
#include <iostream>
#include <string>
#include <vector>
#include "CategoryInh.h"
#include "algorithm"
#include "Message.h"


void Viewer::watchAllMessages(std::vector<CategoryInh> categoryinh) 
{
	for (int i = 0; i < categoryinh.size(); i++)
	{
		std::cout << i + 1;
		categoryinh[i].getMessage();
		std::cout << "/n";
	}
}
void Viewer::sortMessagesByTo(std::vector<CategoryInh> categoryinh)
{
	for (int i = 1; i<categoryinh.size(); i++)
		for (int j = i; j>0 && categoryinh[j - 1].message.getTo() > categoryinh[j].message.getTo(); j--)
			std::swap(categoryinh[j - 1], categoryinh[j]);
			/*std::sort(CategoryInh.begin(), CategoryInh.end(),
		[&](CategoryInh const& message, CategoryInh const& message) { return message.getTo() < message.getTo(); }) */
	watchAllMessages(categoryinh);
}
void Viewer::sortMessagesByFrom(std::vector<CategoryInh> categoryinh)
{
	for (int i = 1; i<categoryinh.size(); i++)
		for (int j = i; j>0 && categoryinh[j - 1].message.getFrom() > categoryinh[j].message.getFrom(); j--)
			std::swap(categoryinh[j - 1], categoryinh[j]);
	watchAllMessages(categoryinh);
}
void Viewer::sortMessagesBySubject(std::vector<CategoryInh> categoryinh)
{
	for (int i = 1; i<categoryinh.size(); i++)
		for (int j = i; j>0 && categoryinh[j - 1].message.getSubject() > categoryinh[j].message.getSubject(); j--)
			std::swap(categoryinh[j - 1], categoryinh[j]);
	watchAllMessages(categoryinh);
}
void Viewer::sortMessagesByDate(std::vector<CategoryInh> categoryinh)
{
	for (int i = 1; i<categoryinh.size(); i++)
		for (int j = i; j>0 && categoryinh[j - 1].message.getDate() > categoryinh[j].message.getDate(); j--)
			std::swap(categoryinh[j - 1], categoryinh[j]);
	watchAllMessages(categoryinh);
}
void Viewer::searchByKeyWord(std::vector<CategoryInh> categoryinh) {
	std::vector<CategoryInh> searchByKeyWordVector;
	std::cout << "������� �������� �����:" << std::endl;
	std::string name;
	std::cin >> name;
	std::cout << std::endl;
	for (int i = 1; i <= categoryinh.size(); i++)
		if (name == categoryinh[i - 1].message.getTo()) {
			CategoryInh X = categoryinh[i - 1]; searchByKeyWordVector.push_back(X);
		}
	watchAllMessages(searchByKeyWordVector);
}
void Viewer:: start(std::vector<CategoryInh>& categoryinh)
{
	int start;
	std::cout << "1 - Watch ALL" << std::endl;
	std::cout << "2 - Sort by SENDER" << std::endl;
	std::cout << "3 - Sort by RECEIVER" << std::endl;
	std::cout << "4 - Sort by SUBJECT" << std::endl;
	std::cout << "5 - Sort by DATE" << std::endl;
	std::cout << "6 - Finish" << std::endl;
	std::cin >> start;
	while (start < 1 && start>10) {
		std::cout << "������� ����� �� 1 �� 10" << std::endl;
		std::cin >> start;
	}
	switch (start) {
	case 1:
		watchAllMessages(categoryinh);
		break;
	case 2:
		sortMessagesByTo(categoryinh);
		break;
	case 3:
		sortMessagesByFrom(categoryinh);
		break;
	case 4:
		sortMessagesBySubject(categoryinh);
		break;
	case 5:
		sortMessagesByDate(categoryinh);
		break;
	case 6:
		break;
	}
}
