#pragma once
#include "vector"
#include "string"
class Message;
class CategoryInh;

class Viewer
{
public:
	void watchAllMessages(std::vector<CategoryInh> categoryinh);
	void sortMessagesByTo(std::vector<CategoryInh> categoryinh);
	void sortMessagesByFrom(std::vector<CategoryInh> categoryinh);
	void sortMessagesBySubject(std::vector<CategoryInh> categoryinh);
	void sortMessagesByDate(std::vector<CategoryInh> categoryinh);
	void searchByKeyWord(std::vector<CategoryInh> categoryinh);
	void start(std::vector<CategoryInh>& category);

};

