#include "stdafx.h"
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include "Message.h"
#include "Category.h"
#include "CategoryInh.h"
#include "Owner.h"
#include "Viewer.h"



int main()
{

	setlocale(LC_ALL, "Russian");

	std::vector<CategoryInh> categoryinh;
	Owner owner;
	Viewer viewer;
	int choice;


	Message MessageOne("vlad.spitkovsky@gmail.com", "vlad.spitkovsky@gmail.com","MyKurs", "20171222");
	CategoryInh tempMessageOne(MessageOne);
	categoryinh.push_back(tempMessageOne);

	Message MessageTwo("vlad.spitkovsky@gmail.com", "i.spitkovsky@gmail.com", "NONE","20171220");
	
	CategoryInh tempMessageTwo(MessageTwo);
	categoryinh.push_back(tempMessageTwo);

	Message MessageThree("r.kipling@gmail.com", "allister@mail.ru","iphone x","20171124");
	CategoryInh tempMessageThree(MessageThree);
	categoryinh.push_back(tempMessageThree);

	Message MessageFour("r.kipling@gmail.com", "allister@mail.ru", "iphone x", "20171124");
	CategoryInh tempMessageFour(MessageFour);
	categoryinh.push_back(tempMessageFour);

	Message MessageFive("ruger@gmail.com", "allister@mail.ru", "iphone x", "20171124");
	CategoryInh tempMessageFive(MessageFive);
	categoryinh.push_back(tempMessageFive);

	Message MessageSix("kipling@gmail.com", "allister@mail.ru", "iphone x", "20171124");
	CategoryInh tempMessageSix(MessageSix);
	categoryinh.push_back(tempMessageSix);

	while (true) {
		std::cout << "Choose controller: 1 - to write message \n";
		std::cout << "Choose controller: 2 - view messages \n";
		std::cin >> choice;

		if (choice == 1) {
			owner.start(categoryinh);
		}
		else if (choice == 2) {
			viewer.start(categoryinh);
		}
		else if (choice == 3) {
			break;
		}
		else {
			std::cout << "ERROR" << std::endl;
		}

	}
	return 0;
}

