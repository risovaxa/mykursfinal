#include "stdafx.h"
#include "CategoryInh.h"
#include "iostream"

CategoryInh::CategoryInh(Message thismessage) : message(thismessage) {};

void CategoryInh::getMessage()
{
	std::cout << std::endl << "From: ";
	std::cout << message.getFrom();
	std::cout << std::endl << "To: ";
	std::cout << message.getTo();
	std::cout << std::endl << "Subject:";
	std::cout << message.getSubject();
	std::cout << std::endl << "Date:";
	std::cout << message.getDate();
	std::cout << "\n";
}
